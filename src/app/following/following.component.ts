import { Component, } from '@angular/core';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.scss']
})
export class FollowingComponent {
  noOfTerms : any;
  main1:boolean=false;
  selectedOption: string="";
  opennn(){
  this.main1=true;
  }

  memberCount: number = 0;
  members: any[] = [];

  addFields() {
      this.members = [];
      for (let i = 0; i < this.memberCount; i++) {
          this.members.push({ id: i + 1, name: '' });
      }
  }
}
