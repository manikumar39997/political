import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { FollowingComponent } from './following/following.component';
import { PostComponent } from './post/post.component';
import { LoginComponent } from './login/login.component';
const routes: Routes = [
  {path:"index",
   component:IndexComponent},
   {path:"follow",
   component:FollowingComponent},
   {path:"post",
   component:PostComponent},
   {path:"login",
   component:LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
